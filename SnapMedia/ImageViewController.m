//
//  ImageViewController.m
//  QuickChat
//
//  Created by Taylor on 12/2/13.
//  Copyright (c) 2013 Taylor Beck. All rights reserved.
//

#import "ImageViewController.h"
#import "RatingVC.h"

@interface ImageViewController ()

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIView *circleView;
@property NSDate *dateOpen;
@property NSDate *dateLoaded;


@property NSNumber *isEphemeral;

@end


@implementation ImageViewController

@synthesize myCounterLabel;


int hours, minutes, seconds;
int secondsLeft;


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Imageview";
    [self setIsEphemeral:[PFUser currentUser][@"isEphemeral"]];
    
    [self setDateOpen:[NSDate date]];
    
    myCounterLabel.hidden = true;
    [self loadImage];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self sendViewData];
    if (arc4random() % 5 == 0) {
        [self segueToRating];
    }
}

- (void)loadImage
{
    [[self activityIndicator] startAnimating];
    dispatch_queue_t imageFetchQ = dispatch_queue_create("image fetch", NULL);
    
    dispatch_async(imageFetchQ, ^{
        PFFile *imageFile = [self.message objectForKey:@"file"];
        NSURL *imageFileUrl = [[NSURL alloc] initWithString:imageFile.url];
        NSData *imageData = [NSData dataWithContentsOfURL:imageFileUrl];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView.image = [UIImage imageWithData:imageData];
            self.textLabel.text = [self.message objectForKey:@"text"];
            
            //NSString *senderName = [self.message objectForKey:@"senderName"];
            //NSString *title = [NSString stringWithFormat:@"Sent from %@", senderName];
            //self.navigationItem.title = title;
            
            [self setDateLoaded:[NSDate date]];
            
            // Countdown Timer
            [[self activityIndicator] stopAnimating];
            if ([[self isEphemeral] isEqualToNumber:@YES]) {
                [self startTimer];
            } else {
                [[self circleView] setHidden: YES];
            }
        });

    });

}

#pragma mark - Helper Methods

- (void)timeout
{
    [timer invalidate];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Countdown Timer Methods
- (void)updateCounter:(NSTimer *)theTimer {
    if(secondsLeft > 0 ){
        secondsLeft -- ;
        seconds = (secondsLeft %3600) % 60;
        myCounterLabel.text = [NSString stringWithFormat:@"%.d", seconds];
    } else {
        [self timeout];
    }
}

-(void)startTimer
{
    myCounterLabel.hidden = false;
    secondsLeft = 6;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Segue to ratings
- (void) segueToRating {
    [self performSegueWithIdentifier:@"ratingSegue" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    RatingVC *ratingVC = (RatingVC*)segue.destinationViewController;
    [ratingVC setImageId:[self.message objectId]];
}

#pragma mark - Send View Data
- (void)sendViewData {
    PFObject *viewData = [PFObject objectWithClassName:@"viewData"];
    [viewData setObject:[[PFUser currentUser] objectId] forKey:@"senderId"];
    //get time since open
    CGFloat diffOpen = [[NSDate date] timeIntervalSinceDate:[self dateOpen]];
    CGFloat diffView = [[NSDate date] timeIntervalSinceDate:[self dateLoaded]];
    if(!isnan(diffOpen) && !isnan(diffView)) {
        [viewData setObject:@(diffOpen) forKey:@"timeSinceOpen"];
        //get time viewed

        [viewData setObject:@(diffView) forKey:@"timeViewed"];
        [viewData setObject:[self.message objectId] forKey:@"imageId"];
        [viewData saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                //oh well...
            }
        }];
    } else {
        NSLog(@"View was not open for long enough");
    }
}

@end
