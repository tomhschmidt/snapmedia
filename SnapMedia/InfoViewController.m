//
//  InfoViewController.m
//  SnapMedia
//
//  Created by Jack Reidy on 5/11/14.
//  Copyright (c) 2014 MediaPop. All rights reserved.
//

#import "InfoViewController.h"

@implementation InfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Info";
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
