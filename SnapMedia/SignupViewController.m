//
//  SignupViewController.m
//  QuickChat
//
//  Created by Taylor on 12/2/13.
//  Copyright (c) 2013 Taylor Beck. All rights reserved.
//

#import "SignupViewController.h"
#import <Parse/Parse.h>
#import "GAIDictionaryBuilder.h"
#import "LoginViewController.h"

@interface SignupViewController ()

@end

@implementation SignupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.hidesBackButton = YES;
    
    
    UISwipeGestureRecognizer *gestureRecognizerSwipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss:)];
    [gestureRecognizerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gestureRecognizerSwipeRight];
    
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    LoginViewController* prevController = [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
    if(prevController) {
        self.usernameField.text = prevController.usernameField.text;
        self.passwordField.text = prevController.passwordField.text;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signup:(id)sender
{
    NSString *username = [self.usernameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *email    = [self.emailField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (username.length == 0 || password.length == 0 || email.length == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                            message:@"Make sure you enter a username, password, and email address!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    } else {
        PFUser *newUser = [PFUser user];
        newUser.username = username;
        newUser.password = password;
        newUser.email = email;
        // random group membership
        NSNumber *isEphemeral = (arc4random() % 2 == 0) ? @NO : @YES;
        [newUser setObject:isEphemeral forKey:@"isEphemeral"];
        
        [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error)
            {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else {
                
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker set:@"&uid" value:[PFUser currentUser].objectId];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"UX" action:@"User Sign In" label:nil value:nil] build]];
                
                
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                [currentInstallation setObject:[PFUser currentUser].objectId forKey:@"owner"];
                [currentInstallation setObject:[PFUser currentUser].objectId forKey:@"userId"];
                
                
                [currentInstallation saveInBackground];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            
        }];
    }
}

- (IBAction)dismiss:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end