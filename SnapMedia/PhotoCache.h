//
//  PhotoCache.h
//  SnapMedia
//
//  Created by Jack Reidy on 5/11/14.
//  Copyright (c) 2014 MediaPop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoCache : NSObject

-(void)updateCacheWithPhoto: (UIImage *)image withID:(NSString*)imageID;
-(BOOL)imageIsCached: (NSString*)imageID;
-(NSData*)getImageWithID: (NSString*)imageID;

@end
