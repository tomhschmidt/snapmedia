//
//  SMAppDelegate.h
//  SnapMedia
//
//  Created by Thomas Schmidt on 4/27/14.
//  Copyright (c) 2014 SnapMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
