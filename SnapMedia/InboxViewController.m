//
//  InboxViewController.m
//  QuickChat
//
//  Created by Taylor on 12/2/13.
//  Copyright (c) 2013 Taylor Beck. All rights reserved.
//

#import "InboxViewController.h"
#import "ImageViewController.h"
#import "MSCellAccessory.h"
#import "GAIDictionaryBuilder.h"

@interface InboxViewController ()

@end

@implementation InboxViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.moviePlayer = [[MPMoviePlayerController alloc]init];
    
    PFUser *currentUser = [PFUser currentUser];
    if (currentUser)
    {
        NSLog(@"Current User: %@", currentUser.username);
    } else
    {
        [self performSegueWithIdentifier:@"showLogin" sender:self];
    }
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.refreshControl addTarget:self action:@selector(retrieveMessages) forControlEvents:UIControlEventValueChanged];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    // Create our Installation query
   // PFQuery *pushQuery1 = [PFInstallation query];
    //[pushQuery1 whereKey:@"deviceType" equalTo:@"ios"];
    
    // Send push notification to query
    //[PFPush sendPushMessageToQueryInBackground:pushQuery1
                                  // withMessage:@"Hello World!"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setHidden:NO];
    
    [self retrieveMessages];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Inbox"];
    [tracker set:[GAIFields customDimensionForIndex:1] value:@"testing"];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.messages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    PFObject *message = [self.messages objectAtIndex:indexPath.row];
    cell.textLabel.text =  [NSDateFormatter localizedStringFromDate:message.createdAt dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle];
    if ([(NSArray *)[message objectForKey:@"receivedIds"] containsObject:[[PFUser currentUser] objectId]]) {
        cell.detailTextLabel.text = @"Viewed";
        cell.detailTextLabel.textColor = [UIColor blackColor];
    } else {
        cell.detailTextLabel.text = @"New";
        cell.detailTextLabel.textColor = [UIColor blueColor];
    }
    
    UIColor *disclosureColor = [UIColor colorWithRed:1 green:0.584 blue:0 alpha:1.0];
    
    cell.accessoryView = [MSCellAccessory accessoryWithType:FLAT_DISCLOSURE_INDICATOR color:disclosureColor];
    
    NSString *fileType = [message objectForKey:@"fileType"];
    
    if ([fileType isEqualToString:@"image"])
    {
        cell.imageView.image = [UIImage imageNamed:@"icon_image"];
    } else {
        cell.imageView.image = [UIImage imageNamed:@"icon_video"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedMessage  = [self.messages objectAtIndex:indexPath.row];
    
    NSString *fileType = [self.selectedMessage objectForKey:@"fileType"];
    
    if ([fileType isEqualToString:@"image"])
    {
        [self performSegueWithIdentifier:@"showImage" sender:self];
    }
    
    NSMutableArray *receivedIds = [NSMutableArray arrayWithArray:[self.selectedMessage objectForKey:@"receivedIds"]];
    
    if (!receivedIds || ![receivedIds containsObject:[[PFUser currentUser] objectId]]) {
        [receivedIds addObject:[[PFUser currentUser] objectId]];
        [[self selectedMessage] setObject:receivedIds forKey:@"receivedIds"];
        [[self selectedMessage] saveInBackground];
    }
}

- (IBAction)logout:(id)sender
{
    [PFUser logOut];
    [self performSegueWithIdentifier:@"showLogin" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showLogin"]) {
        [segue.destinationViewController setHidesBottomBarWhenPushed:YES];
    } else if ([segue.identifier isEqualToString:@"showImage"]) {
        [segue.destinationViewController setHidesBottomBarWhenPushed:YES];
        
        ImageViewController *imageViewController = (ImageViewController *)segue.destinationViewController;
        imageViewController.message = self.selectedMessage;
    }
}

#pragma mark - Helper Methods

- (void)retrieveMessages{
    [self retrieveMessages:nil];
}

- (void)retrieveMessages:(void (^)(UIBackgroundFetchResult))handler
{
    PFQuery *query = [PFQuery queryWithClassName:@"Messages"];
    //8[query whereKey:@"recipientIds" equalTo:[[PFUser currentUser] objectId]];
    if ([[[PFUser currentUser] objectForKey:@"isEphemeral"] isEqualToNumber: @YES]) {
        [query whereKey:@"receivedIds" notEqualTo:[[PFUser currentUser] objectId]];
    }
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            NSLog(@"%@ %@", error, [error userInfo]);
            if(handler) {
                handler(UIBackgroundFetchResultFailed);
            }
        } else {
            self.messages = objects;
            
            [self.tableView reloadData];
            NSLog(@"Retrieved %lu Messages", (unsigned long)[self.messages count]);
            if(handler) {
                handler(UIBackgroundFetchResultNewData);
            }
        }
        
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        
    }];
}


- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
