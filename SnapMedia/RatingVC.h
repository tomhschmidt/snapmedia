//
//  RatingVC.h
//  SnapMedia
//
//  Created by Jack Reidy on 5/13/14.
//  Copyright (c) 2014 MediaPop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingVC : GAITrackedViewController

@property (nonatomic) NSString *imageId;

@end
