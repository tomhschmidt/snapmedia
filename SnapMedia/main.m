//
//  main.m
//  SnapMedia
//
//  Created by Thomas Schmidt on 4/27/14.
//  Copyright (c) 2014 SnapMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SMAppDelegate class]));
    }
}
