//
//  CircleView.m
//  SnapMedia
//
//  Created by Jack Reidy on 5/11/14.
//  Copyright (c) 2014 MediaPop. All rights reserved.
//

#import "CircleView.h"

@implementation CircleView

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

-(void) drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(ctx, rect);
    CGContextSetFillColor(ctx, CGColorGetComponents([[UIColor colorWithRed:67.f/255.f green:120.f/255.f blue:206.f/255.f alpha:1.0f] CGColor]));
    CGContextFillPath(ctx);
}

@end
