//
//  PhotoCache.m
//  SnapMedia
//
//  Created by Jack Reidy on 5/11/14.
//  Copyright (c) 2014 MediaPop. All rights reserved.
//

#import "PhotoCache.h"

@interface PhotoCache()

@property (nonatomic) NSString *recentPhotosPath;

@end

@implementation PhotoCache

//the following functions operate on the file system.
//they allow interested parties to write and read images
//from a directory in the device's file system.

//here we lazily instantiate the path to our directory of photos
-(NSString*)recentPhotosPath
{
    if (!_recentPhotosPath) {
        NSString *documentDirectory =
        [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        _recentPhotosPath = [documentDirectory stringByAppendingPathComponent:@"CachedPicturePopPhotos"];
    }
    return _recentPhotosPath;
}

//this function determines whether or not an image is
//stored in the cache.
-(BOOL)imageIsCached: (NSString*)imageID
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSString *imagePath = [self imagePathForID:imageID];
    return [fileManager fileExistsAtPath:imagePath];
}

//this function returns an image with a given
//ID or nil if the image is not found
-(NSData*)getImageWithID: (NSString*)imageID
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSString *imagePath = [self imagePathForID:imageID];
    return [fileManager contentsAtPath:imagePath];
}

//this function adds a photo to the cache, removing the oldest photos
//from the cache if necessary
-(void)addPhotoToCache: (UIImage*)image withID:(NSString*)imageID
{
    NSLog(@"add: %@", imageID);
    if (![self imageIsCached:imageID]) {
        NSLog(@"this image is not in the directory");
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        if (![fileManager fileExistsAtPath:[self recentPhotosPath]]) {
            NSLog(@"directory does not exist");
            NSError *error;
            [fileManager createDirectoryAtPath:[self recentPhotosPath] withIntermediateDirectories:YES attributes:nil error:&error];
            NSLog(@"%@", [error description]);
        }
        NSData *imageData = UIImagePNGRepresentation(image);
        NSString *imagePath = [self imagePathForID:imageID];
        
        [fileManager createFileAtPath:imagePath contents:imageData attributes:nil];
    }
}

//this little funciton reduces redundant code :)
-(NSString *)imagePathForID: (NSString*)imageID
{
    NSString *imageFileName = [NSString stringWithFormat:@"%@.png", imageID];
    NSString *imagePath = [[self recentPhotosPath] stringByAppendingPathComponent:imageFileName];
    return imagePath;
}

#define IPHONE_CACHE_SIZE 8 //enough for about 5 photos at FlickrPhotoFormatLarge
#define IPAD_CACHE_SIZE 16 //enough for about 5 photos at FlickrPhotoFormatOriginal

-(void)updateCacheWithPhoto: (UIImage *)image withID:(NSString*)imageID
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSLog(@"update: %@", imageID);
    [self addPhotoToCache:image withID:imageID];
    NSLog(@"%@", [fileManager contentsOfDirectoryAtPath:[self recentPhotosPath] error:nil]);
    NSLog(@"directory size: %lli", [self directorySize]);
    
    int cacheSize = 0;
    //this is a nifty way of determining which type of device
    //we're on without having to specify particular models
    NSString *deviceType = [UIDevice currentDevice].model;
    if ([deviceType rangeOfString:@"iPhone"].location == NSNotFound) {
        NSLog(@"we're using an iPad");
        cacheSize = IPAD_CACHE_SIZE * pow(10, 6);
    } else {
        NSLog(@"we're using an iPhone");
        cacheSize = IPHONE_CACHE_SIZE * pow(10, 6);
    }
    
    while ([self directorySize] > cacheSize) {
        NSLog(@"too many images, removing oldest: \n%@", [fileManager contentsOfDirectoryAtPath:[self recentPhotosPath] error:nil]);
        [self removeOldestPhotoFromCache];
    }
    NSLog(@"existing directory after update: \n%@", [fileManager contentsOfDirectoryAtPath:[self recentPhotosPath] error:nil]);
    
    
    //    NSLog(@"%@", [fileManager contentsOfDirectoryAtPath:[self recentPhotosPath] error:nil]);
    //    [self emptyCache];
    //    NSLog(@"%@", [fileManager contentsOfDirectoryAtPath:[self recentPhotosPath] error:nil]);
}

-(void)removeOldestPhotoFromCache
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *filesArray = [fileManager subpathsOfDirectoryAtPath:[self recentPhotosPath] error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    NSDate *oldestDate;
    NSString *imageID;
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [fileManager attributesOfItemAtPath:[[self recentPhotosPath] stringByAppendingPathComponent:fileName] error:nil];
        NSDate *fileDate = [fileDictionary fileModificationDate];
        if (!oldestDate || [fileDate compare:oldestDate] == NSOrderedAscending) {
            oldestDate = fileDate;
            imageID = [fileName substringToIndex:[fileName length] - 4];
            NSLog(@"%@", imageID);
        }
    }
    [self removePhotoFromCache:imageID];
}

//this funciton is meant to be a secret for updateCacheWithPhoto alone.
//Images are removed from the cache only when others are to be added.
-(void)removePhotoFromCache: (NSString*)imageID
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSString *imagePath = [self imagePathForID:imageID];
    NSError *error;
    [fileManager removeItemAtPath:imagePath error:&error];
    NSLog(@"%@", [error description]);
}

//this function wipes the directory and exists solely for the
//programmer's convenience
-(void)emptyCache
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    [fileManager removeItemAtPath:[self recentPhotosPath] error:nil];
}

-(unsigned long long int)directorySize
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *filesArray = [fileManager subpathsOfDirectoryAtPath:[self recentPhotosPath] error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [fileManager attributesOfItemAtPath:[[self recentPhotosPath] stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    return fileSize;
}

@end
