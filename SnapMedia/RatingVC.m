//
//  RatingVC.m
//  SnapMedia
//
//  Created by Jack Reidy on 5/13/14.
//  Copyright (c) 2014 MediaPop. All rights reserved.
//

#import "RatingVC.h"
#import <Parse/Parse.h>

@interface RatingVC()

@property (strong, nonatomic) IBOutlet UISegmentedControl *ratingSegmentControl;
@property (strong, nonatomic) IBOutlet UIButton *returnButton;
@property NSDate *dateOpen;
@property (strong, nonatomic) IBOutlet UILabel *ratingText;

@property (weak, nonatomic) IBOutlet UILabel *lowEndLabel;
@property (weak, nonatomic) IBOutlet UILabel *highEndLabel;
@property NSString* displayedText;
@property BOOL enjoymentTextDisplayed;

@end

@implementation RatingVC

NSString* qualityText = @"Rate the quality of the image you viewed.";
NSString* enjoymentText = @"Rate how much you liked this image.";


- (void)viewWillAppear:(BOOL)animated {
    if (arc4random() % 2 == 0) {
        _enjoymentTextDisplayed = true;
        [_lowEndLabel setHidden:YES];
        [_highEndLabel setHidden:YES];
        _ratingText.text = enjoymentText;
    } else {
        _enjoymentTextDisplayed = false;
        [_lowEndLabel setHidden:NO];
        [_highEndLabel setHidden:NO];
        _ratingText.text = qualityText;
    }
}

-(void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"Ratingview";
    [[self returnButton] setHidden:YES];
    [self setDateOpen:[NSDate date]];
    
    [[self ratingSegmentControl] setSelectedSegmentIndex:UISegmentedControlNoSegment];
}

- (IBAction)ratingSegmentControlChanged:(UISegmentedControl *)sender {
    [[self returnButton] setHidden:NO];
}


- (IBAction)returnButtonPressed:(UIButton *)sender {
    PFObject *ratingData = [PFObject objectWithClassName:@"ratingData"];
    [ratingData setObject:[[PFUser currentUser] objectId] forKey:@"senderId"];
    //get time since open
    CGFloat diffOpen = [[NSDate date] timeIntervalSinceDate:[self dateOpen]];
    [ratingData setObject:@(diffOpen) forKey:@"timeSinceOpen"];
    [ratingData setObject:[self imageId] forKey:@"imageId"];
    NSNumber *rating = [NSNumber numberWithInteger:[[self ratingSegmentControl] selectedSegmentIndex] + 1];
    [ratingData setObject:rating forKey:@"rating"];
    if(_enjoymentTextDisplayed) {
        [ratingData setObject:@"enjoyment" forKey:@"metric"];
    } else {
        [ratingData setObject:@"quality" forKey:@"metric"];
    }
    [ratingData saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            //oh well...
        }
    }];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
